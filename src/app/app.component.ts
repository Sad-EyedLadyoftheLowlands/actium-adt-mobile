import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';

import {Plugins, PushNotification, PushNotificationActionPerformed, PushNotificationToken} from '@capacitor/core';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {Platform} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {InjectableRxStompConfig} from '@stomp/ng2-stompjs';

import {RxStompService} from '@stomp/ng2-stompjs';
import {Message, StompHeaders} from '@stomp/stompjs';

import {Subscription} from 'rxjs';
import {AuthUser} from './models/user.model';

import {AuthService} from './services/auth.service';
import {MqService} from './services/mq.service';
import {STORAGE_KEYS} from './services/storage.service';
import {StorageService} from './services/storage.service';

const {PushNotifications} = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'actium-adt-test';

  appPages = [];

  lblHome: string;
  lblTasks: string;
  lblSettings: string;
  lblHelp: string;
  lblLegend: string;
  lblAbout: string;

  // Observables
  authenticated: boolean;
  user: AuthUser;
  private authSubscription: Subscription;
  private userSubscription: Subscription;

  public message: any;
  public receivedMessages: string[] = [];
  private topicSubscription: Subscription;
  private binding = '/exchange/JmdExchange/renai.*';
  private destination = '/exchange/JmdExchange/renai.general';
  private subscriptionHeaders: StompHeaders;

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private translate: TranslateService,
    private rxStompService: RxStompService,
    private stompConfig: InjectableRxStompConfig,
    private authService: AuthService,
    private mqService: MqService,
    private storageService: StorageService
  ) {
    translate.addLangs(['fr', 'en']);
    translate.setDefaultLang('fr');
    translate.use('fr');
    this.authenticated = (this.authService.isAuthenticated === true ? true : false);
    this.user = this.authService.currentUser;
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    if (!this.platform.is('desktop')) {
      // Firebase
      // Request permission to use push notifications
      // iOS will prompt user and return if they granted permission or not
      // Android will just grant without prompting
      PushNotifications.requestPermission().then(result => {
        if (result.granted) {
          // Register with Apple / Google to receive push via APNS/FCM
          PushNotifications.register();
        } else {
          // Show some error
        }
      });

      // On success, we should be able to receive notifications
      PushNotifications.addListener('registration',
        (token: PushNotificationToken) => {
          this.storageService.lsSetStringValue('firebaseToken', token.value);
          console.log('Push registration success, token: ' + token.value);
        }
      );

      // Some issue with our setup and push will not work
      PushNotifications.addListener('registrationError',
        (error: any) => {
          console.log('Error on registration: ' + JSON.stringify(error));
        }
      );

      // Show us the notification payload if the app is open on our device
      PushNotifications.addListener('pushNotificationReceived',
        (notification: PushNotification) => {
          console.log('Push received: ' + JSON.stringify(notification));
        }
      );

      // Method called when tapping on a notification
      PushNotifications.addListener('pushNotificationActionPerformed',
        (notification: PushNotificationActionPerformed) => {
          console.log('Push action performed: ' + JSON.stringify(notification));
        }
      );
    }

    // Translation
    this.translate.get(['app.menu.home', 'app.menu.tasks', 'app.menu.settings', 'app.menu.help', 'app.menu.legend', 'app.menu.about-us'])
      .subscribe(translations => {
        this.lblHome = translations['app.menu.home'];
        this.lblTasks = translations['app.menu.tasks'];
        this.lblSettings = translations['app.menu.settings'];
        this.lblHelp = translations['app.menu.help'];
        this.lblLegend = translations['app.menu.legend'];
        this.lblAbout = translations['app.menu.about-us'];
        this.setItems();
      });

    this.authSubscription = this.authService.isAuthObservable.subscribe(data => {
      if (this.authenticated !== data) {
        this.authenticated = data;
        this.setItems();
      }
    });
    this.userSubscription = this.authService.userObservable.subscribe(data => {
      this.user = data;
    });

    // RabbitMQ
    const mqQueueName = `ionic-${this.getUniqueId(2)}`;
    this.storageService.lsSetStringValue('mqQueueName', mqQueueName);
    this.subscriptionHeaders = {
      id: 'angular',
      'x-queue-name': mqQueueName
    };
    const mqUrl = this.storageService.lsGetStringValue(STORAGE_KEYS.MQ_URL, 'wss://dev-adt.actiumtechnologie.com:15673/ws');
    if (mqUrl) {
      this.stompConfig.brokerURL = mqUrl;
      console.log('configuring stomp', this.stompConfig);
      this.rxStompService.configure(this.stompConfig);
    }
    console.log('Subscription to stomp Service');
    this.topicSubscription = this.rxStompService
      .watch(this.binding, this.subscriptionHeaders)
      .subscribe((message: Message) => {
      // console.log(message.body);
      this.receivedMessages.push(message.body);
      const obj = JSON.parse(message.body);
      this.mqService.dispatchMessage(obj);
    });

  }

  ngOnDestroy() {
    this.authSubscription.unsubscribe();
    this.userSubscription.unsubscribe();
    this.topicSubscription.unsubscribe();
  }

  setItems() {
    console.log('data from observable: ', this.authenticated);
    console.log('isAuthenticated: ', this.authService.isAuthenticated);
    if (this.authenticated || this.authService.isAuthenticated) {
      this.authenticated = true;
    }
    this.appPages = [
      {
        title: this.lblHome,
        url: '/auth',
        icon: 'log-in-outline'
      },
      {
        title: this.lblTasks,
        url: '/tasks',
        icon: 'list-outline'
      },
      {
        title: this.lblSettings,
        url: '/settings',
        icon: 'build'
      }
    ];
  }

  logout() {
    console.log('AppComponent::logout (user): ', this.user);
    if (this.user) {
      this.authService.logout(this.user).subscribe(
        data => {
          console.log('Logged Out');
        },
        error => {
          console.log(error);
        },
        () => {
          this.router.navigateByUrl('/auth');
        }
      );
    }
  }

  getUniqueId(parts: number): string {
    const stringArr = [];
    for (let i = 0; i < parts; i++) {
      // tslint:disable-next-line:no-bitwise
      const S4 = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
      stringArr.push(S4);
    }
    return stringArr.join('-');
  }
}

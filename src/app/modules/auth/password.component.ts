import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ModalController} from '@ionic/angular';
import {TranslateService} from '@ngx-translate/core';
import {ResetPassword} from 'src/app/models/password.model';
import {AlertService} from 'src/app/services/alert.service';
import {AuthService} from 'src/app/services/auth.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss']
})
export class PasswordComponent implements OnInit {

  public email: string = '';
  public resetForm: FormGroup;
  public resetPassword: ResetPassword = null;
  public sent: boolean = false;
  lblCodeRequestSent: string;
  lblPasswordChanged: string;
  lblErrorOccured: string;

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private alertService: AlertService,
    private translate: TranslateService
  ) {
    this.resetPassword = {
      resetCode: '',
      password: '',
      repeat: ''
    };
    this.resetForm = this.formBuilder.group({
      resetCode: [this.resetPassword.resetCode, Validators.required],
      password: [this.resetPassword.password, Validators.required],
      repeat: [this.resetPassword.repeat, Validators.required]
    });
    translate.use('fr');
  }

  ngOnInit() {
    // Translation
    this.translate.get(['modules.auth.code-request-sent', 'modules.auth.password-changed', 'modules.auth.error-occured'])
      .subscribe(translations => {
        this.lblCodeRequestSent = translations['modules.auth.code-request-sent'];
        this.lblPasswordChanged = translations['modules.auth.password-changed'];
        this.lblErrorOccured = translations['modules.auth.error-occured'];
      });
  }

  requestResetCode() {
    // console.log(this.email);
    this.authService.requestResetCode(this.email).subscribe(
      data => {
        this.alertService.presentToast(this.lblCodeRequestSent);
        this.sent = true;
      },
      error => {
        console.log(error);
        this.alertService.presentToast(this.lblErrorOccured + error.message);
      },
      () => {
        //
      }
    );
  }

  reinitPassword(form: FormGroup) {
    this.resetPassword.email = this.email;
    this.resetPassword.resetCode = form.value.resetCode;
    this.resetPassword.password = form.value.password;
    this.resetPassword.repeat = form.value.repeat;
    this.authService.reinitPassword(this.resetPassword).subscribe(
      data => {
        this.alertService.presentToast(this.lblPasswordChanged);
      },
      error => {
        console.log(error);
        this.alertService.presentToast(this.lblErrorOccured + error.message);
      },
      () => {
        //
      }
    );
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }

}

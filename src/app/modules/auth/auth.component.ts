import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {MenuController, ModalController, NavController} from '@ionic/angular';
import {AuthUser} from 'src/app/models/user.model';
import {AlertService} from 'src/app/services/alert.service';

import {AuthService} from 'src/app/services/auth.service';
import {DataService} from 'src/app/services/data.service';
import {SettingsService} from 'src/app/services/settings.service';
import {StorageService} from 'src/app/services/storage.service';
import {PasswordComponent} from './password.component';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {//
  public user: AuthUser = null;
  public shifts: any;
  public routes: any;
  public errorMsg: string;
  public firebaseToken: string;

  public loginForm: FormGroup;

  constructor(
    private activatedRoute: ActivatedRoute,
    private modalController: ModalController,
    private menu: MenuController,
    private router: Router,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private storageService: StorageService,
    private settingsService: SettingsService,
    private dataService: DataService,
    private alertService: AlertService,
    private navCtrl: NavController,
  ) {
    // this.menu.enable(false);
    this.user = {
      username: '',
      password: '',
      shift: 0,
      route: 0
    };
    this.loginForm = this.formBuilder.group({
      username: [this.user.username, Validators.required],
      password: [this.user.password, Validators.required],
      shift: [null, Validators.required],
      route: [null, Validators.required]
    });
  }

  ionViewWillEnter() {
    console.log('AuthComponent::ionViewWillEnter');
    if (!this.shifts) {
      this.getSettings();
    }
  }

  ngOnInit() {
    console.log('AuthComponent::ngOnInit');
    this.getSettings();
  }

  getSettings() {
    console.log('AuthComponent::getSettings');
    this.settingsService.getCurentSettings().then((value: any) => {
      const settings = value;
      console.log('AuthComponent::getSettings (settings): ', settings);
      if (settings) {
        if (!settings.validated) {
          this.router.navigate(['/settings']);
        } else {
          const isAuth = this.authService.isAuthenticated;
          console.log('AuthComponent::getSettings (auth): ', isAuth);
          if (isAuth === true) {
            this.router.navigate(['/tasks']);
          } else {
            this.getShifts();
          }
        }
      } else {
        console.log('before navigate');
        this.router.navigate(['/settings']);
        // this.router.navigateByUrl('/settings');
      }
    });
  }

  getShifts() {
    this.dataService.getShifts().subscribe(
      (data) => {
        // console.log(data);
        this.shifts = data;
        // this.alertService.presentToast("getShifts (data): " + data);
      },
      (error) => {
        console.log(error);
        this.alertService.presentToast('getShifts (error): ' + error.message);
      },
      () => {
        // console.log("shifts query completed");
        // this.logMessage("shifts query completed");
      }
    );
  }

  login(form: FormGroup) {
    this.user.username = form.value.username;
    this.user.password = form.value.password;
    this.user.shift = form.value.shift.id;
    this.user.route = form.value.route.id;
    console.log('user: ', this.user);
    this.authService.login(this.user).subscribe(
      data => {
        this.alertService.presentToast('Logged In');
        this.authService.saveFirebaseToken(this.user.username, this.storageService.lsGetStringValue('firebaseToken'));
      },
      error => {
        console.log(error);
        this.alertService.presentToast('login (error): ' + error.message);
      },
      () => {
        this.navCtrl.navigateRoot('/tasks');
      }
    );
  }

  shiftsFn(event: any): void {
    // console.log(event.detail.value);
    this.loginForm.patchValue({route: null});
    this.dataService.getRoutes(event.detail.value.id).subscribe(
      data => {
        // console.log(data);
        this.routes = data;
        // this.alertService.presentToast("Received");
      },
      error => {
        console.log(error);
        this.alertService.presentToast('shiftsFn (error): ' + error.message);
      },
      () => {
        // console.log("routes query completed");
      }
    );
  }

  routesFn(event: any): void {
    // console.log(event.detail.value);
  }

  async forgotPassword() {
    const modal = await this.modalController.create({
      component: PasswordComponent,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

}

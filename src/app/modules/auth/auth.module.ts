import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Routes} from '@angular/router';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';

import {AuthRoutingModule} from './auth-routing.module';

import {AuthComponent} from './auth.component';
import {PasswordComponent} from './password.component';

const routes: Routes = [
  {
    path: '',
    component: AuthComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AuthRoutingModule,
    TranslateModule.forChild()
  ],
  entryComponents: [PasswordComponent],
  declarations: [AuthComponent, PasswordComponent]
})
export class AuthModule {
}

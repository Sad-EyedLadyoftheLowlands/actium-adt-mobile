import {Component, OnDestroy, OnInit} from '@angular/core';
import {DomSanitizer} from '@angular/platform-browser';
import {NavigationExtras, Router} from '@angular/router';
import {CameraResultType, Plugins} from '@capacitor/core';
import {MenuController, ModalController} from '@ionic/angular';
import {Subscription} from 'rxjs';
import {AlertService} from 'src/app/services/alert.service';
import {AuthService} from 'src/app/services/auth.service';
import {DataService} from 'src/app/services/data.service';
import {MqService} from 'src/app/services/mq.service';

const {Camera} = Plugins;
const {Filesystem} = Plugins;

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})
export class TaskComponent implements OnInit, OnDestroy {

  constructor(
    private menu: MenuController,
    private modalController: ModalController,
    private router: Router,
    private sanitizer: DomSanitizer,
    private authService: AuthService,
    private dataService: DataService,
    private alertService: AlertService,
    private mqService: MqService
  ) {
    this.menu.enable(true);
    // this.task = this.router.getCurrentNavigation().extras.state; //JMD replaced by modal instead of navigation
  }
  user: any;
  task: any;
  currentAction: any;
  reasons: any;
  reasonsVisible: boolean;
  infections: any;

  private mqMessageSubscription: Subscription;

  converted_image: any;

  ngOnInit() {
    this.reasonsVisible = false;
    /*this.reasons = [
        {"id": 1, "label": "Déjà sur une tâche"},
        {"id": 2, "label": "En pause"},
        {"id": 3, "label": "Fin de quart"}
    ];*/
    this.getReasons();
    this.getInfections();
    this.mqMessageSubscription = this.mqService.currentMessage.subscribe((message: any) => {
      this.onMqMessage(message);
    });
  }

  ionViewWillEnter() {
    //
  }

  ngOnDestroy() {
    this.mqMessageSubscription.unsubscribe();
  }

  onMqMessage(message: any) {
    if (message.msgType === 1) { // task message
      // console.log('TASK_MESSAGE: ', message);
      if (message.id === this.task.id) {
        this.dataService.getTask(message.id).subscribe((data: any) => {
          // console.log('new task: ', data);
          this.task = data;
        });
      }
    }
  }

  getReasons() {
    this.dataService.getReasons().subscribe(
      data => {
        console.log(data);
        this.reasons = data;
        // this.alertService.presentToast("Received");
      },
      error => {
        console.log(error);
        this.alertService.presentToast(error.message);
      },
      () => {
        console.log('reasons query completed');
      }
    );
  }

  getInfections() {
    this.dataService.getInfections().subscribe(
      data => {
        console.log(data);
        this.infections = data;
        // this.alertService.presentToast("Received");
      },
      error => {
        console.log(error);
        this.alertService.presentToast(error.message);
      },
      () => {
        console.log('infections query completed');
      }
    );
  }

  getInfectionLabel(id: number) {
    if (this.infections) {
      const infection = this.infections.find(obj => {
        return obj.id === id;
      });
      if (infection) {
        return infection.label
          ;
      } else {
        return '';
      }
    }
  }

  getInfectionStyle(id: number) {
    if (this.infections) {
      const infection = this.infections.find(obj => {
        return obj.id === id;
      });
      if (infection) {
        return `color: #${infection.color}; background-color: #${infection.bgColor}`;
      } else {
        return '';
      }
    }
  }

  actionClicked(action) {
    console.log(action);
    this.currentAction = action;
    if (!action.hasReason) {
      this.completeTask(this.task, action, null);
    } else {
      this.reasonsVisible = true;
      setTimeout(() => {
        const el = document.getElementById(`reasons`);
        el.setAttribute('style', 'visibility:visible');
      }, 500);
    }
  }

  completeTask(task, action, reason) {
    // this.alertService.presentToast(`task:${task.dbId}, action:${action.label}, reason:${reason}`);
    console.log(`task:${task.id}, action:${action.label}, reason:${reason}`);
    this.dataService.completeTask(task, action, reason).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log(error);
        this.alertService.presentToast(error.message);
      },
      () => {
        console.log('task update completed');
        const navigationExtras: NavigationExtras = {
          state: task
        };
        // this.router.navigate([`/tasks`], navigationExtras); //JMD replaced with modal dismiss
        this.dismiss();
      }
    );
  }

  setReason(target) {
    this.completeTask(this.task, this.currentAction, target.value);
  }

  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri
    });
    // image.webPath will contain a path that can be set as an image src.
    // You can access the original file using image.path, which can be
    // passed to the Filesystem API to read the raw data of the image,
    // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)//

    this.converted_image = image.webPath;
    // this.converted_image ="data:image/jpeg;base64," + this.sanitizer.bypassSecurityTrustResourceUrl(image.base64String);
    // this.converted_image = this.sanitizer.bypassSecurityTrustResourceUrl(image.base64String);

    // Can be set to the src of an image now
    const photo: any = document.getElementById(`photo`);
    photo.setAttribute('style', 'visibility:visible');
    // photo.src = imageUrl;

    const imgData = await Filesystem.readFile({
      path: image.path
    });
    console.log('data', imgData);

    const imgBlob = new Blob([imgData.data], {type: 'image/jpeg'});
    console.log('blob', imgBlob);

    this.dataService.uploadPhoto(this.task.id, imgBlob, `${this.task.id}.jpg`).subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log(error);
        this.alertService.presentToast(error.message);
      },
      () => {
        console.log('photo upload completed');
      }
    );
  }

  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalController.dismiss({
      dismissed: true
    });
  }

}

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {TranslateModule} from '@ngx-translate/core';

import {SafePipe} from '../../pipes/safe.pipe';
import {TaskComponent} from './task.component';

import {TasksRoutingModule} from './tasks-routing.module';

import {TasksComponent} from './tasks.component';

/* const routes: Routes = [
  {
    path: '',
    component: TasksComponent
  }
]; */

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TasksRoutingModule,
    TranslateModule.forChild()
  ],
  declarations: [
    TasksComponent,
    TaskComponent,
    SafePipe
  ],
  entryComponents: [TaskComponent],
  exports: [
    SafePipe
  ]
})
export class TasksModule {
}

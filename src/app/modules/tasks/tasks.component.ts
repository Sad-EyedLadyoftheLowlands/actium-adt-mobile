import {Component, OnDestroy, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MenuController, ModalController} from '@ionic/angular';
import {Subscription} from 'rxjs';
import {AlertService} from 'src/app/services/alert.service';
import {AuthService} from 'src/app/services/auth.service';
import {DataService} from 'src/app/services/data.service';
import {MqService} from 'src/app/services/mq.service';
import {TaskComponent} from './task.component';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
})
export class TasksComponent implements OnInit, OnDestroy {
  user: any;
  tasks: any;
  currentTask: any;
  currentAction: any;
  reasons: any;
  reasonsVisible: boolean;
  infections: any;

  private mqMessageSubscription: Subscription;

  constructor(
      private menu: MenuController,
      private modalController: ModalController,
      private router: Router,
      private authService: AuthService,
      private dataService: DataService,
      private alertService: AlertService,
      private mqService: MqService
  ) {
    this.menu.enable(true);
  }

  ngOnInit() {
    this.reasonsVisible = false;
    this.getReasons();
    this.getInfections();
    this.mqMessageSubscription = this.mqService.currentMessage.subscribe((message: any) => {
      this.onMqMessage(message);
    });
  }

  ionViewWillEnter() {
    this.user = this.authService.currentUser;
    if (this.user) {
      this.getTasks();
    }
  }

  ngOnDestroy() {
    this.mqMessageSubscription.unsubscribe();
  }

  onMqMessage(message: any) {
    if (message.msgType === 1) { // task message
      console.log('TASK_MESSAGE: ', message);
      if (this.tasks) {
        this.dataService.getTask(message.id).subscribe((data: any) => {
          console.log('tasks before', this.tasks.length, JSON.stringify(this.tasks));
          const task = data;
          // db - all task messages were being spliced into tasks
          // this check simply ensures that only those messages pertaining to the user are spliced
          // this method may need to be revisited
          if (task.assignee === this.authService.currentUser.username) {
            // message.id always > 0
            let found: boolean = false;
            this.tasks.forEach((item, index) => {
              if (item.id === task.id) {
                console.log('removing found task at index', index);
                found = true;
                // known status
                if (task.taskStatus === 0 || task.taskStatus === 0) {
                  this.tasks.splice(index, 1, task);
                } else {
                  this.tasks.splice(index, 1);
                }
              }
            });
            console.log('tasks between', this.tasks.length, JSON.stringify(this.tasks));
            if (!found) {
              console.log('adding task because not found');
              this.tasks.push(task);
            }
            this.tasks = [...this.tasks];
            console.log('tasks end', this.tasks.length, JSON.stringify(this.tasks));
          }
        });
      }
    }
  }
  /*
                    const idx = this.tasks.findIndex(obj => {
                      return obj.id === task.id;
                    });
                    if (idx !== null && idx !== undefined && idx >= 0) { // if (idx >= 0) {
                      if (task.taskStatus === 0 || task.taskStatus === 1) { // running or paused
                        this.tasks.splice(idx, 1, task);
                      } else {
                        this.tasks.splice(idx, 1);
                      }
                      this.tasks.splice(idx, 1); // why remove again??
                      this.tasks = [...this.tasks];
                      console.log('updated task: ', this.tasks[idx]);
                    }
                    if (idx < 0) {
                      this.tasks.push(task);
                      this.tasks = [...this.tasks];
                      console.log('new task: ', task);
                    }
          */

  getReasons() {
    this.dataService.getReasons().subscribe(
        data => {
          console.log(data);
          this.reasons = data;
          // this.alertService.presentToast("Received");
        },
        error => {
          console.log(error);
          this.alertService.presentToast(error.message);
        },
        () => {
          console.log('reasons query completed');
        }
    );
  }

  getInfections() {
    this.dataService.getInfections().subscribe(
        data => {
          console.log(data);
          this.infections = data;
          // this.alertService.presentToast("Received");
        },
        error => {
          console.log(error);
          this.alertService.presentToast(error.message);
        },
        () => {
          console.log('infections query completed');
        }
    );
  }

  getInfectionLabel(id: number) {
    if (this.infections) {
      const infection = this.infections.find(obj => {
        return obj.id === id;
      });
      if (infection) {
        return infection.label
            ;
      } else {
        return '';
      }
    }
  }

  getInfectionStyle(id: number) {
    if (this.infections) {
      const infection = this.infections.find(obj => {
        return obj.id === id;
      });
      if (infection) {
        return `color: #${infection.color}; background-color: #${infection.bgColor}`;
      } else {
        return '';
      }
    }
  }

  getTasks() {
    if (this.user) {
      this.dataService.getTasks(this.user.username).subscribe(
          data => {
            console.log(data);
            this.tasks = data;
            // this.alertService.presentToast("Received");
          },
          error => {
            console.log(error);
            this.alertService.presentToast(error.message);
          },
          () => {
            console.log('tasks query completed');
          }
      );
    }
  }

  /* actionClicked(task, action) {
    this.currentTask = task;
    console.log(task);
    this.currentAction = action;
    console.log(action);
    if(!action.hasReason) {
      this.completeTask(task, action, null);
    }
    else {
      this.reasonsVisible = true;
      setTimeout(()=> {
        var el = document.getElementById(`r-${task.id}`);
        el.setAttribute("style", "visibility:visible");
      },500);
    }
  } */

  /*taskClicked(task) {
      const navigationExtras: NavigationExtras = {
          state: task
      };
      this.router.navigate([`/tasks/1`], navigationExtras);
  }*/

  async taskClicked(task) {
    const modal = await this.modalController.create({
      component: TaskComponent,
      cssClass: 'my-custom-class',
      componentProps: {
        task: task
      }
    });
    return await modal.present();
  }

  completeTask(task, action, reason) {
    // this.alertService.presentToast(`task:${task.dbId}, action:${action.label}, reason:${reason}`);
    console.log(`task:${task.id}, action:${action.label}, reason:${reason}`);
    /* this.dataService.completeTask(task, action, reason).subscribe(
    data => {
        console.log(data);
    },
    error => {
        console.log(error);
        this.alertService.presentToast(error.message);
    },
    () => {
        console.log("task update completed");
        this.getTasks();
    }
    ); */
  }

  setReason(target) {
    this.completeTask(this.currentTask, this.currentAction, target.value);
  }

  swiped(task, event) {
    // console.log(`swiped ${event.target.id}`, event.detail);
    const el = document.getElementById(`r-${task.id}`);
    if (el != null && event.detail.amount < 50) {
      el.setAttribute('style', 'visibility:hidden');
      this.reasonsVisible = false;
    }
  }
}

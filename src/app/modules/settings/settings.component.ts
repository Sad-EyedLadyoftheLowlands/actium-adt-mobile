import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Settings} from 'src/app/models/settings.model';
import {AlertService} from 'src/app/services/alert.service';
import {SettingsService} from 'src/app/services/settings.service';
import {STORAGE_KEYS} from '../../services/storage.service';
import {StorageService} from '../../services/storage.service';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss'],
})
export class SettingsComponent implements OnInit {

  public settings: Settings = null;

  public settingsForm: FormGroup;

  lblBadServer: string;
  langs: any;

  constructor(
    public router: Router,
    private formBuilder: FormBuilder,
    private translate: TranslateService,
    public alertService: AlertService,
    public settingsService: SettingsService,
    private storageService: StorageService
  ) {
    this.langs = [
      {
        value: 'fr',
        label: 'FR'
      },
      {
        value: 'en',
        label: 'EN'
      }
    ];
    this.settings = {
      username: '',
      url: '',
      validated: false,
      lang: 'fr',
      channel: 'main'
    };
  }

  ionViewWillEnter() {
  }

  ngOnInit() {
    this.translate.get(['modules.settings.bad-server'])
      .subscribe(translations => {
        this.lblBadServer = translations['modules.settings.bad-server'];
      });
    /* let tmpSettings = this.settingsService.currentSettings;
    console.log(tmpSettings);
    if(tmpSettings != null && tmpSettings != undefined) {
        this.settings = tmpSettings;
    } */
    this.settingsForm = this.formBuilder.group({
      // username: ['', Validators.required],
      url: ['', Validators.required],
      validated: [false],
      lang: ['fr']
    });
    this.settingsService.getCurentSettings().then((settings: any) => {
      const tmpSettings = settings;
      if (tmpSettings) {
        this.settings = tmpSettings;
      }
      this.settingsForm.patchValue({username: this.settings.username});
      this.settingsForm.patchValue({url: this.settings.url});
      this.settingsForm.patchValue({validated: this.settings.validated});
      this.settingsForm.patchValue({lang: this.settings.lang});
    });
  }

  save(form: FormGroup) {
    this.settingsService.currentSettings = form.value;
    this.settingsService.about(form.value.url)
      .subscribe(
      data => {
        if (data.name === 'Actium ADT' && data.version === '2.0') {
          this.alertService.presentToast('OK');
          this.settingsForm.patchValue({validated: true});
          this.settingsService.currentSettings = form.value;
          // Extract mqurl from response
          if (data.mqurl) {
            this.storageService.lsSetStringValue(STORAGE_KEYS.MQ_URL, data.mqurl);
          }
          this.router.navigate(['/auth']);
        } else {
          this.alertService.presentToast(this.lblBadServer);
        }
      },
      error => {
        console.log('error: ', error);
        // this.alertService.presentToast("save (error): " + error.message);
        this.alertService.presentToast(this.lblBadServer);
      },
      () => {
        // console.log("save query completed");
      }
    );
  }

  flush() {
    this.settingsService.flush();
    this.settings = null;
    this.settingsForm.patchValue({username: ''});
    this.settingsForm.patchValue({url: ''});
    this.settingsForm.patchValue({validated: false});
    this.settingsForm.patchValue({lang: ''});
  }

}

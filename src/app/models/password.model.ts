export interface ResetPassword {
    resetCode: string;
    password: string;
    repeat: string;
    email?: string;
}

export interface TokenResponse {
  userid: number;
  token: string;
  refreshtoken: string;
}

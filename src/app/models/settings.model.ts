export interface Settings {
    username: string;
    url: string;
    validated: boolean;
    lang: string;
    channel: string;
}

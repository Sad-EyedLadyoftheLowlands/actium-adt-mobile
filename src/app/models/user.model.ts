export interface AuthUser {
  username: string;
  password: string;
  repeat?: string;
  shift?: number;
  route?: number;
  firebaseToken?: string;
  firstName?: string;
  lastName?: string;
  email?: string;
}

export interface CodeResponse {
    success?: boolean;
    code?: string;
}

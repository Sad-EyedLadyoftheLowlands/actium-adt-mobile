export interface Shift {
  id: number;
  name: string;
  day: string;
  fromHM: string;
  toHM: string;
  shiftroute: [];
  routes: [];
}

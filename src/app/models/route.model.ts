export interface Route {
    id: number;
    name: string;
    routeroom: [];
    shiftroute: [];
    rooms: [];
    employees: [];
}

import {InjectableRxStompConfig} from '@stomp/ng2-stompjs';

export const AdtRxStompConfig: InjectableRxStompConfig = {
  // Which server?
  // brokerURL: 'ws://192.168.2.46:15674/ws',
  brokerURL: 'wss://dev-adt.actiumtechnologie.com:15673/ws',

  // Headers
  // Typical keys: login, passcode, host
  // we must provide non-generic to avoid  "login failed - access_refused (user must access over loopback)""
  connectHeaders: {
    login: 'angular',
    passcode: 'demo',
    'client-id': 'adt-mobile'
  },

  // How often to heartbeat?
  // Interval in milliseconds, set to 0 to disable
  heartbeatIncoming: 0, // Typical value 0 - disabled
  heartbeatOutgoing: 0, // Typical value 20000 - every 20 seconds

  // Wait in milliseconds before attempting auto reconnect
  // Set to 0 to disable
  // Typical value 5000 (5 seconds)
  reconnectDelay: 200,

  // Will log diagnostics on console
  // It can be quite verbose, not recommended in production
  // Skip this key to stop logging to console
  debug: (msg: string): void => {
    // console.log(new Date(), msg);
  }
};

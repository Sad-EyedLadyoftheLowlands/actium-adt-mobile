import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable, Subscription} from 'rxjs';
import {SettingsService} from '../services/settings.service';

@Injectable({
  providedIn: 'root'
})
export class SettingsGuard implements CanActivate {

  settings: any;
  private settingsSubscription: Subscription;

  constructor(
    private router: Router,
    private settingsService: SettingsService
  ) {
    this.settingsSubscription = this.settingsService.settingsObservable.subscribe(data => {
      this.settings = data;
    });
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    // state: RouterStateSnapshot): any {
    const settings = this.settings;
    if (settings) {
      // if(settings.url != null && settings.url != undefined && settings.url != "") {
      if (settings.validated) {
        // configured, return true
        return true;
      }
    }
    // not configured correctly, redirect to settings page
    this.router.navigate(['/settings']);
    return false;
    /* this.settingsService.getCurentSettings().then((value: any) => {
        const settings = value;
        if(settings != null && settings != undefined) {
            if(settings.url != null && settings.url != undefined && settings.url != "") {
                // configured, return true
                return true;
            }
        }
        // not configured correctly, redirect to settings page
        this.router.navigate(['/settings']);
        return false;
    }); */
  }
}

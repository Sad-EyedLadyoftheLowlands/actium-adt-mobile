import {
  HttpErrorResponse,
  HttpHandler,
  HttpHeaderResponse,
  HttpInterceptor,
  HttpProgressEvent,
  HttpRequest,
  HttpResponse,
  HttpSentEvent,
  HttpUserEvent
} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, throwError, TimeoutError} from 'rxjs';
import {catchError, filter, finalize, switchMap, take} from 'rxjs/operators';
import {Config} from '../config/env.config';
import {TokenResponse} from '../models/token-response.model';
import {AuthService} from '../services/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {
  }

  isRefreshingToken: boolean = false;
  tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpSentEvent | HttpHeaderResponse
    | HttpProgressEvent | HttpResponse<any> | HttpUserEvent<any> | any> {

    return next.handle(this.addTokenToRequest(request, this.authService.accessToken))
      .pipe(
        // timeout(5000),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch ((err as HttpErrorResponse).status) {
              case 401:
                return this.handle401Error(request, next);
              case 400:
                const user = this.authService.currentUser;
                if (user) {
                  return this.authService.logout(user) as any;
                } else {
                  return next.handle(request);
                }
              case 404:
                return next.handle(request);
            }
          } else if (err instanceof TimeoutError) {
            return next.handle(request);
          } else {
            return throwError(err);
          }
        }));
  }

  private addTokenToRequest(request: HttpRequest<any>, token: string): HttpRequest<any> {
    return request.clone({setHeaders: {Authorization: `${Config.tokenType} ${token}`}, setParams: {lang: 'fr'}});
  }

  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {

    if (!this.isRefreshingToken) {
      this.isRefreshingToken = true;

      // Reset here so that the following requests wait until the token
      // comes back from the refreshToken call.
      this.tokenSubject.next(null);

      return this.authService.refresh()
        .pipe(
          switchMap((tokenResponse: TokenResponse) => {
            if (tokenResponse) {
              this.tokenSubject.next(tokenResponse.token);
              return next.handle(this.addTokenToRequest(request, tokenResponse.token));
            }
            const user = this.authService.currentUser;
            return this.authService.logout(user) as any;
          }),
          catchError(err => {
            const user = this.authService.currentUser;
            return this.authService.logout(user) as any;
          }),
          finalize(() => {
            this.isRefreshingToken = false;
          })
        );
    } else {
      this.isRefreshingToken = false;

      return this.tokenSubject
        .pipe(filter(token => token != null),
          take(1),
          switchMap(token => {
            return next.handle(this.addTokenToRequest(request, token));
          }));
    }
  }
}

import {Injectable} from '@angular/core';
import {Plugins} from '@capacitor/core';

const {Storage} = Plugins;

export const STORAGE_KEYS = {
  MQ_URL : 'mqUrl'
};

@Injectable()
export class StorageService {

  // localStorage
  lsSetStringValue(key: string, value: string): void {
    localStorage.setItem(key, value);
  }

  lsGetStringValue(key: string, defaultValue?: string): string {
    if (localStorage.getItem(key) != null) {
      return localStorage.getItem(key);
    } else {
      return defaultValue;
    }
  }

  lsSetBooleanValue(key: string, value: boolean): void {
    const valueStr = JSON.stringify(value);
    localStorage.setItem(key, valueStr);
  }

  lsGetBooleanValue(key: string, defaultValue?: boolean): boolean {
    if (localStorage.getItem(key) != null) {
      const valueStr = localStorage.getItem(key);
      return JSON.parse(valueStr);
    } else {
      return defaultValue;
    }
  }

  lsSetNumberValue(key: string, value: number): void {
    const valueStr = JSON.stringify(value);
    localStorage.setItem(key, valueStr);
  }

  lsGetNumberValue(key: string, defaultValue?: number): number {
    if (localStorage.getItem(key) != null) {
      const valueStr = localStorage.getItem(key);
      return JSON.parse(valueStr);
    } else {
      return defaultValue;
    }
  }

  lsSetObject<T>(key: string, value: T): void {
    const valueStr = JSON.stringify(value);
    localStorage.setItem(key, valueStr);
  }

  lsGetObject<T>(key: string): T {
    const valueStr = localStorage.getItem(key);
    if (valueStr) {
      return JSON.parse(valueStr);
    } else {
      return undefined;
    }
  }

  lsRemoveItem(key: string): void {
    localStorage.removeItem(key);
  }

  lsClear(): void {
    localStorage.clear();
  }

  // Capacitor Storage
  csSetStringValue(key: string, value: string): void {
    Storage.set({key: key, value: value});
  }

  async csGetStringValue(key: string, defaultValue?: string): Promise<string> {
    const value = await Storage.get({key: key});
    if (value != null) {
      return value.value;
    } else {
      return defaultValue;
    }
  }

  csSetBooleanValue(key: string, value: boolean): void {
    const valueStr = JSON.stringify(value);
    Storage.set({key: key, value: valueStr});
  }

  async csGetBooleanValue(key: string, defaultValue?: boolean): Promise<boolean> {
    const value = await Storage.get({key: key});
    if (value != null) {
      return JSON.parse(value.value);
    } else {
      return defaultValue;
    }
  }

  csSetNumberValue(key: string, value: number): void {
    const valueStr = JSON.stringify(value);
    Storage.set({key: key, value: valueStr});
  }

  async csGetNumberValue(key: string, defaultValue?: number): Promise<number> {
    const value = await Storage.get({key: key});
    if (value != null) {
      return JSON.parse(value.value);
    } else {
      return defaultValue;
    }
  }

  csSetObject<T>(key: string, value: T): void {
    const valueStr = JSON.stringify(value);
    Storage.set({key: key, value: valueStr});
  }

  async csGetObject<T>(key: string): Promise<T> {
    const valueStr = await Storage.get({key: key});
    return JSON.parse(valueStr.value);
  }

  csRemoveItem(key: string): void {
    Storage.remove({key: key});
  }

  csClear(): void {
    Storage.clear();
  }
}

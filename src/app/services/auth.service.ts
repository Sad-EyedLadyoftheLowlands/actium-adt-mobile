import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {tap} from 'rxjs/operators';
import {Config} from '../config/env.config';
import {CodeResponse} from '../models/code-response.model';
import {ResetPassword} from '../models/password.model';
import {TokenResponse} from '../models/token-response.model';
import {AuthUser} from '../models/user.model';
import {SettingsService} from './settings.service';
import {StorageService} from './storage.service';

declare var require: any;
const jwtDecode = require('jwt-decode');

const AUTHENTICATED_KEY = 'AUTHENTICATED';
const ACCESS_TOKEN_KEY = 'ACCESS_TOKEN';
const TOKEN_EXPIRATION_KEY = 'TOKEN_EXPIRATION';
const REFRESH_TOKEN_KEY = 'REFRESH_TOKEN';
const CURRENT_USER_KEY = 'CURRENT_USER';

@Injectable()
export class AuthService {

  public get isAuthenticated() {
    const isAuth = this.storageService.lsGetBooleanValue(AUTHENTICATED_KEY);
    this.isAuthSource.next(isAuth);
    return isAuth;
  }

  public set isAuthenticated(isAuth: boolean) {
    this.storageService.lsSetBooleanValue(AUTHENTICATED_KEY, isAuth);
    this.isAuthSource.next(isAuth);
  }

  public get accessToken() {
    return this.storageService.lsGetStringValue(ACCESS_TOKEN_KEY);
  }

  public set accessToken(val: string) {
    this.storageService.lsSetStringValue(ACCESS_TOKEN_KEY, val);
  }

  public get tokenExpiration() {
    return this.storageService.lsGetNumberValue(TOKEN_EXPIRATION_KEY);
  }

  public set tokenExpiration(val: number) {
    this.storageService.lsSetNumberValue(TOKEN_EXPIRATION_KEY, val);
  }

  public get refreshToken() {
    return this.storageService.lsGetStringValue(REFRESH_TOKEN_KEY);
  }

  public set refreshToken(val: string) {
    this.storageService.lsSetStringValue(REFRESH_TOKEN_KEY, val);
  }

  public get currentUser() {
    const user: AuthUser = this.storageService.lsGetObject(CURRENT_USER_KEY);
    this.userSource.next(user);
    return user;
  }

  public set currentUser(user: AuthUser) {
    this.storageService.lsSetObject(CURRENT_USER_KEY, user);
    this.userSource.next(user);
  }

  // Observables
  _authenticated: any;
  _user: any;
  isAuthSource = new BehaviorSubject(false);
  isAuthObservable = this.isAuthSource.asObservable();
  userSource = new BehaviorSubject(this._user);
  userObservable = this.userSource.asObservable();
  settings: any;
  private settingsSubscription: Subscription;

  constructor(
    private http: HttpClient,
    private storageService: StorageService,
    private settingsService: SettingsService
  ) {
    this._user = null;
    this.settingsSubscription = this.settingsService.settingsObservable.subscribe(data => {
      this.settings = data;
    });
  }

  public getShifts(): Observable<any> {
    // console.log("authservice:getShifts");
    return this.http.get(`${this.settings.url}/api/actutil/shifts`).pipe(
      tap(shifts => {
        // console.dir(shifts);
        // return shifts;
      })
    );
  }

  public getRoutes(id: number): Observable<any> {
    // console.log("authservice:getRoutes");
    return this.http.get(`${this.settings.url}/api/util/shifts/${id}/routes`).pipe(
      tap(routes => {
        // console.dir(routes);
        // return routes;
      })
    );
  }

  public login(user: AuthUser): Observable<any> {
    return this.http.post(`${this.settings.url}/api/acttoken`, {
      ...user
    }).pipe(
      tap((tokenResponse: TokenResponse) => {
        // console.dir(result);
        const decoded = jwtDecode(tokenResponse.token);
        // console.dir(decoded);
        user.lastName = decoded.LastName;
        user.firstName = decoded.FirstName;
        user.email = decoded.Email;
        this.currentUser = user;
        /* this.zone.run(() => {
            this.store.set('currentUser', user);
        });
        //this.store.set('currentUser', user); */
        this.isAuthenticated = true;
        this.accessToken = tokenResponse.token;
        this.tokenExpiration = Number(decoded.exp);
        this.refreshToken = tokenResponse.refreshtoken;
      })
    );
  }

  public refresh(): Observable<any> {
    return this.http.post(`${this.settings.url}/api/acttoken/refresh`, {
      Token: this.accessToken, RefreshToken: this.refreshToken
    }).pipe(
      tap((tokenResponse: TokenResponse) => {
        let user: AuthUser;
        user = this.storageService.lsGetObject('CURRENT_USER_KEY');
        const decoded = jwtDecode(tokenResponse.token);
        // console.dir(decoded);
        user.lastName = decoded.LastName;
        user.firstName = decoded.FirstName;
        user.email = decoded.Email;
        this.storageService.lsSetObject('CURRENT_USER_KEY', user);
        this.isAuthenticated = true;
        this.accessToken = tokenResponse.token;
        this.tokenExpiration = Number(decoded.exp);
        this.refreshToken = tokenResponse.refreshtoken;

      })
    );
  }

  logout(user: any): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `${Config.tokenType} ${this.accessToken}`
    });
    console.log(`logout for ${user.username}`);
    return this.http.post(`${this.settings.url}/api/acttoken/exit`,
      {username: user.username}, {headers: headers})
      .pipe(
        tap(data => {
          console.log('data: ', data);
          this.isAuthenticated = false;
          this.storageService.lsRemoveItem(AUTHENTICATED_KEY);
          this.storageService.lsRemoveItem(ACCESS_TOKEN_KEY);
          this.storageService.lsRemoveItem(TOKEN_EXPIRATION_KEY);
          this.storageService.lsRemoveItem(REFRESH_TOKEN_KEY);
          this.storageService.lsRemoveItem(CURRENT_USER_KEY);
          return data;
        })
      );

  }

  saveFirebaseToken(username: string, token: string) {
    const body = {
      Username: username,
      FirebaseToken: token
    };
    return this.http.post(`${this.settings.url}/api/acttoken/firebase`, {
      ...body
    }).pipe(
      tap(data => {
        return data;
      }),
    );
  }

  public requestResetCode(email: string): Observable<any> {
    const body = {
      email: email
    };
    return this.http.post(`${this.settings.url}/api/actuser/code`, {
      ...body
    }).pipe(
      tap((codeResponse: CodeResponse) => {
        console.dir(codeResponse);
      })
    );
  }

  public reinitPassword(resetPassword: ResetPassword): Observable<any> {
    console.dir(resetPassword);
    return this.http.post(`${this.settings.url}/api/actuser/password`, {
      ...resetPassword
    }).pipe(
      tap((success: boolean) => {
        console.dir(success);
      })
    );
  }

}

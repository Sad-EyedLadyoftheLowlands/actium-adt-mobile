import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable, Subscription} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AlertService} from './alert.service';
import {SettingsService} from './settings.service';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  // observable
  settings: any;
  private settingsSubscription: Subscription;

  constructor(
    private http: HttpClient,
    private alertService: AlertService,
    private settingsService: SettingsService
  ) {
    this.settingsSubscription = this.settingsService.settingsObservable.subscribe(data => {
      this.settings = data;
    });
  }

  getShifts(): Observable<any> {
    // this.alertService.presentToast("data.service:getShifts");
    return this.http.get(`${this.settings.url}/api/actroute/shifts`).pipe(
      tap(shifts => {
        // this.alertService.presentToast("data.service:getShifts:tap");
        return shifts;
      }),
    );
  }

  getRoutes(id: number): Observable<any> {
    return this.http.get(`${this.settings.url}/api/actroute/shifts/${id}/routes`).pipe(
      tap(routes => {
        return routes;
      }),
    );
  }

  getReasons(): Observable<any> {
    return this.http.get(`${this.settings.url}/api/actreason/allreasons`).pipe(
      tap(reasons => {
        return reasons;
      }),
    );
  }

  getInfections(): Observable<any> {
    return this.http.get(`${this.settings.url}/api/actinfection/infections`).pipe(
      tap(infections => {
        return infections;
      }),
    );
  }

  getTasks(username: string): Observable<any> {
    return this.http.get(`${this.settings.url}/api/acttask?filters=t.status IN(0) AND assignee='${username}'&orderBy=id`)
      .pipe(
        tap(tasks => {
          return tasks;
        }),
      );
  }

  getTask(id: number): Observable<any> {
    return this.http.get(`${this.settings.url}/api/acttask/${id}`).pipe(
      tap(task => {
        return task;
      }),
    );
  }

  public completeTask(task: any, action: any, reason: any): Observable<any> {
    const body: any = {
      nodeName: task.name,
      action: action.name,
      assignee: task.assignee
    };
    if (reason) {
      body.reasonId = reason.id;
    }
    return this.http.patch<any>(`${this.settings.url}/api/acttask/${task.id}/complete`, body).pipe(
      tap(data => {
        return data;
      }),
    );
  }

  uploadPhoto(taskId: number, photo: any, fileName: string) {
    const formData = new FormData();
    formData.append('file', photo, fileName);
    return this.http.post(`${this.settings.url}/api/acttask/${taskId}/photo`, formData).pipe(
      tap(data => {
        console.log(data);
      })
    );
  }

}

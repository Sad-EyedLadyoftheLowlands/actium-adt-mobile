import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';

import {Plugins} from '@capacitor/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {timeout} from 'rxjs/operators';
import {Settings} from '../models/settings.model';
import {AlertService} from './alert.service';
import {StorageService} from './storage.service';

const CURRENT_CONFIG_KEY = 'CURRENT_CONFIG';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  /* public get currentSettings() {
      return this.storageService.lsGetObject(CURRENT_CONFIG_KEY);
  } */

  public set currentSettings(settings: Settings) {
    this.storageService.csSetObject(CURRENT_CONFIG_KEY, settings);
    this.settingsSource.next(settings);
  }

  constructor(
    private http: HttpClient,
    // private nhttp: HTTP,
    private storageService: StorageService,
    private alertService: AlertService
  ) {
    this.settings = null;
    this.getCurentSettings().then(() => {
      // just to be sure settings is setup and available for other services
    });
  }

  settings: Settings;

  settingsSource = new BehaviorSubject(this.settings);
  settingsObservable = this.settingsSource.asObservable();

  async getCurentSettings(): Promise<Settings> {
    const settings: Settings = await this.storageService.csGetObject(CURRENT_CONFIG_KEY);
    this.settings = settings;
    this.settingsSource.next(settings);
    return settings;
  }

  public flush() {
    this.storageService.csRemoveItem(CURRENT_CONFIG_KEY);
    this.settings = null;
    this.settingsSource.next(null);
  }

  about(url: string): Observable<any> {
    return this.http.get(`${url}/api/actutil/about`).pipe(
      timeout(5000)
    );
  }
}

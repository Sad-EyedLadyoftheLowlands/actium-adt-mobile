import {Injectable} from '@angular/core';
import {Deploy} from 'cordova-plugin-ionic/dist/ngx';
import {SettingsService} from './settings.service';

@Injectable({
  providedIn: 'root'
})
export class UpdateService {

  constructor(private settingsService: SettingsService,
              private deploy: Deploy,
              ) {

    this.settingsService.settingsObservable
      .subscribe(settings => {
        if (settings) {
          console.log('starting service check');
          if (settings.channel) {
            this.startUpdateService(settings.channel).then(r => console.log('update service configured'));
          }
        }
      });
  }

  private async startUpdateService(channel: string) {
    await this.deploy.configure({channel: channel});
    const update = await this.deploy.checkForUpdate();
    if (update.available) {
      await this.deploy.downloadUpdate();
      await this.deploy.extractUpdate();
      // await this.deploy.reloadApp();
    }
  }
}

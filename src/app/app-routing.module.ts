import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './utils/auth.guard';

const routes: Routes = [
  // { path: '', redirectTo: 'folder/Inbox', pathMatch: 'full' },
  { path: '', redirectTo: 'tasks', pathMatch: 'full' },
  { path: 'auth', loadChildren: () => import('./modules/auth/auth.module').then((m) => m.AuthModule) },
  { path: 'tasks', loadChildren: () => import('./modules/tasks/tasks.module').then((m) => m.TasksModule), canActivate: [AuthGuard] },
  // { path: "tasks", loadChildren: () => import("./modules/tasks/tasks.module").then((m) => m.TasksModule) },
  { path: 'about', loadChildren: () => import('./modules/about/about.module').then((m) => m.AboutModule) },
  { path: 'settings', loadChildren: () => import('./modules/settings/settings.module').then((m) => m.SettingsModule) },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then( m => m.FolderPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
